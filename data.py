# data.py

import datetime
import yfinance as yf

def get_stock_data(ticker_list):
    now = datetime.datetime.now()
    year_ago = now - datetime.timedelta(days=365)

    data = yf.download(
        ticker_list, 
        start = year_ago.strftime("%Y-%m-%d"), 
        end=now.strftime("%Y-%m-%d")
    )

    return data
