# PyScript Test Repo

This repository contains dummy code to play, explore and present how the brand-new (in 2022) 
[PyScript] framework works.

**ATTENTION**

As disclaimed on the project website:

> PyScript is very alpha and under heavy development. There are many known issues, from usability 
to loading times, and you should expect things to change often. We encourage people to play and 
explore with PyScript, but at this time, **we do not recommend using it for production**.

## Order of learning and testing

Please follow the below order to have a constructive (incremental) idea of how to explore and play 
with [PyScript]:

1. [hello.html](hello.html)
2. [pi.html](pi.html)
3. [today_pi.html](pi.html)
4. [plot.html](plot.html)
5. [stock_prices.html](stock_prices.html)

## How to run

To run the code examples of this repository, just clone this repo and execute each of the HTML
files in your browser.

For the [stock_prices.html](stock_prices.html), you must have a WebServer to serve additional
files. Use [this hack](https://github.com/pyscript/pyscript/issues/257#issuecomment-1119595062) to 
handle that.

<!-- For those examples inner its own directory structure, follow the execution steps described in their
README.md file. -->

## References

The following references were used to code these tests:

1. [PyScript Getting Started]
2. [PyScript Demos] 

[PyScript]: https://pyscript.net/ 
[PyScript Getting Started]: https://github.com/pyscript/pyscript/blob/main/docs/tutorials/getting-started.md
[PyScript Demos]: https://pyscript.net/examples/
